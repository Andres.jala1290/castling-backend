import { Coordinate } from '../core/coordinate';
import { BLACK_PIECES, WHITE_PIECES } from '../core/teamColor';
import Piece from './piece.interface';
import Bishop from './bishop.model';
import Horse from './horse.model';
import King from './king.model';
import Pawn from './pawn.model';
import Queen from './queen.model';
import Rook from './rook.model';
import fillBoard from '../core/boardFiller';

export default class Board {
  private board: Map<string, Piece>;

  constructor() {
    this.board = fillBoard();
  }

  getPiece(pieceCoordinates: Coordinate) {
    return this.board.get(pieceCoordinates.toKey());
  }

  movePiece(movingPieceCoordinate: Coordinate, targetedSpace: Coordinate) {
    if (
      this.board
        .get(movingPieceCoordinate.toKey())
        ?.isValidMove(movingPieceCoordinate, targetedSpace)
    ) {
      if (this.IsThereEnemy(movingPieceCoordinate, targetedSpace)) {
        this.board.delete(movingPieceCoordinate.toKey());
      }

      const movingPiece = this.board.get(
        movingPieceCoordinate.toKey()
      ) as Piece;
      this.board.delete(movingPieceCoordinate.toKey());
      this.board.set(targetedSpace.toKey(), movingPiece);
    }
  }

  IsThereEnemy(movingPieceCoordinate: Coordinate, targetedStace: Coordinate) {
    if (this.board.has(targetedStace.toKey())) {
      const teamColor = this.board
        .get(movingPieceCoordinate.toKey())
        ?.getTeam();

      return this.board.get(targetedStace.toKey())?.getTeam() == teamColor;
    }
  }
}
