import { Coordinate } from '../core/coordinate';

export default interface Piece {
  getTeam(): string;

  isValidMove(position: Coordinate, target: Coordinate): boolean;
}
