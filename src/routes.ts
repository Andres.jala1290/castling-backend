import { Express, Request, Response } from 'express';
import { manageGameHandler } from './controllers/game.controller';

function routes(app: Express) {
  app.get('/healthcheck', (request: Request, response: Response) =>
    response.sendStatus(200)
  );

  app.post('/api/game', manageGameHandler);

  app.post('/api/team', manageGameHandler);
}

export default routes;
