import { DocumentDefinition } from 'mongoose';
import Game from '../models/game.model';
import GameModel from '../models/game.model';

let activeGame: Game;

export async function createGame() {
  try {
    activeGame = new Game();
    return await GameModel.create();
  } catch (error: any) {
    throw new Error(error);
  }
}
